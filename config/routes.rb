Rails.application.routes.draw do
  root to: 'dashboard#show'
  get 'locked' => 'pages#locked', as: :locked_screen
  get 'thankyou' => 'pages#thankyou', as: :thankyou_screen

  devise_for :staffs
  devise_for :users

  resources :tickets do
    member do
      put :feedback_submit
    end
    collection do
      get :search
    end
    resources :comments
  end

  resources :todos
  resources :products do
    collection do
        get :search
      end
  end
  resources :quotes
  resource :account, controller: :account
  resource :staff_account, controller: :staff_account
  resources :notifications do
    collection do
      put :mark_as_read
    end
  end

  namespace :manager do
    resource :dashboard, controller: :dashboard do
      collection do
        get :chart_data
      end
    end
    resources :users
  end
 

  namespace :admin do
    resources :staffs
    
  end

  namespace :guest do
    resources :tickets
  end
  
  namespace :staff do
    resources :quotes do
      member do
        post :create_ticket
      end
    end



    resource :dashboard, controller: :dashboard do
      collection do
        get :chart_data
      end
    end
    resources :tickets do
      member do
        put :feedback_submit
      end
      collection do
        get :search
      end
    end
    resources :clients do
      resources :users do
      collection do
        get :search
      end
    end
 
    end
  end

end
