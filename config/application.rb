require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TicketingSystem
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    
    config.action_view.field_error_proc = Proc.new { |html_tag, instance|
      if html_tag.include? 'class="'
        html_tag.gsub('class="', 'class="field_with_errors ').html_safe
      else
        html_tag.gsub(/^\<[^\s]*/){|s| s + ' class="field_with_errors"'}.html_safe
      end
    }
  end
end
