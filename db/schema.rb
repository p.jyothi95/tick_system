# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180731135812) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.integer "product_id"
    t.string "location"
    t.string "managed_by"
    t.string "department"
    t.string "used_by"
    t.date "assigned_on"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone"
    t.string "email"
    t.string "website"
    t.boolean "is_deleted", default: false
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "comments", force: :cascade do |t|
    t.integer "ticket_id"
    t.text "description"
    t.boolean "is_private", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "creator_id"
    t.string "creator_type"
  end

  create_table "hardware_details", force: :cascade do |t|
    t.integer "product_id"
    t.string "processor"
    t.string "hdd"
    t.string "ram"
    t.string "serial_no"
    t.date "purchase_date"
    t.integer "warranty"
    t.text "add_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string "reference_type"
    t.integer "reference_id"
    t.string "notifiable_type"
    t.integer "notifiable_id"
    t.string "notification_type"
    t.text "detail"
    t.boolean "is_read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "asset_type"
    t.string "brand"
    t.integer "asset_tag"
    t.integer "quantity"
    t.integer "model_no"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.string "zip_code", limit: 50
    t.string "phone", limit: 15
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "product"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "preferred_method"
    t.text "description"
    t.integer "ticket_id"
  end

  create_table "software_details", force: :cascade do |t|
    t.integer "product_id"
    t.string "software_os_name"
    t.string "product_key"
    t.date "purchase_date"
    t.date "expiry_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staffs", force: :cascade do |t|
    t.string "name"
    t.string "role", limit: 10
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean "is_deleted", default: false
    t.index ["email"], name: "index_staffs_on_email", unique: true
    t.index ["reset_password_token"], name: "index_staffs_on_reset_password_token", unique: true
  end

  create_table "ticket_logs", force: :cascade do |t|
    t.integer "ticket_id"
    t.string "action", limit: 20
    t.integer "performer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reference_id"
    t.string "reference_type"
    t.text "details"
    t.string "performer_type"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "name"
    t.string "category"
    t.string "subject"
    t.text "description"
    t.string "status", limit: 20
    t.integer "user_id"
    t.integer "staff_id"
    t.date "expected_closing_date"
    t.string "priority", limit: 20
    t.string "source", limit: 20
    t.string "service_type", limit: 20
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_guest", default: false
    t.text "extra"
    t.integer "feedback_score"
    t.text "feedback_comment"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "todos", force: :cascade do |t|
    t.text "description"
    t.boolean "is_completed", default: false
    t.integer "creator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "creator_type"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.integer "client_id"
    t.boolean "is_deleted", default: false
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
