class AddDescriptionToQuotes < ActiveRecord::Migration[5.1]
  def change
    add_column :quotes, :description, :text
  end
end
