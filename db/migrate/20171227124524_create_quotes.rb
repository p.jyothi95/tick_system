class CreateQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :quotes do |t|
      t.integer :user_id
      t.string :name
      t.string :category, limit: 50
      t.string :phone, limit: 15
      t.string :email
      t.text :requirement
      t.timestamps
    end
  end
end
