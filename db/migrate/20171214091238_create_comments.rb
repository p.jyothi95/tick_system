class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer :ticket_id
      t.text :description
      t.boolean :is_private, default: false
      t.timestamps
    end
  end
end
