class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.text :description
      t.boolean :is_completed, default: false
      t.integer :user_id
      t.timestamps
    end
  end
end
