class AddDetailsToTicketLogs < ActiveRecord::Migration[5.1]
  def change
    add_column :ticket_logs, :details, :text
  end
end
