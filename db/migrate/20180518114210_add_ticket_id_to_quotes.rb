class AddTicketIdToQuotes < ActiveRecord::Migration[5.1]
  def change
    add_column :quotes, :ticket_id, :integer
  end
end
