class AddColumnToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :is_guest, :boolean, default: false
    add_column :tickets, :extra, :text
  end
end
