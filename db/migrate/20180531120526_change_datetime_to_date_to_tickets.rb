class ChangeDatetimeToDateToTickets < ActiveRecord::Migration[5.1]
  def change
    change_column :tickets, :expected_closing_date, :date
  end
end
