class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.string :name
      t.string :category
      t.string :subject
      t.text :description
      t.string :status, limit: 20
      t.integer :creator_id
      t.integer :assignee_id
      t.datetime :expected_closing_date
      t.string :priority, limit: 20 
      t.string :source, limit: 20
      t.string :service_type, limit: 20
      t.timestamps
    end
  end
end
