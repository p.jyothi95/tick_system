class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.string :reference_type
      t.integer :reference_id
      t.string :notifiable_type
      t.integer :notifiable_id
      t.string :notification_type
      t.text :detail
      t.boolean :is_read, default: false
      t.timestamps
    end
  end
end
