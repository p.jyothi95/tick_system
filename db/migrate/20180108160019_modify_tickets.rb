class ModifyTickets < ActiveRecord::Migration[5.1]
  def change
    rename_column :tickets, :creator_id, :user_id
    rename_column :tickets, :assignee_id, :staff_id
  end
end
