class RemoveDeptColumnFromProducts < ActiveRecord::Migration[5.1]
  def change
    remove_column :products, :department
  end
end
