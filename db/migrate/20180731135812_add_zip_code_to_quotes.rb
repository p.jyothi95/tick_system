class AddZipCodeToQuotes < ActiveRecord::Migration[5.1]
  def change
    add_column :quotes, :zip_code, :string, limit: 10
  end
end
