class CreateTicketLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :ticket_logs do |t|
      t.integer :ticket_id
      t.string :action, limit: 20
      t.integer :user_id
      t.timestamps
    end
  end
end
