class AddColumnToClients < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :phone, :string
    add_column :clients, :email, :string
    add_column :clients, :website, :string
    add_column :clients, :is_deleted, :boolean, default: false
     add_attachment :clients, :logo
  end
end
