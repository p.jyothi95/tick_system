class AddReferenceToTicketLogs < ActiveRecord::Migration[5.1]
  def change
    add_column :ticket_logs, :reference_id, :integer
    add_column :ticket_logs, :reference_type, :string
  end
end
