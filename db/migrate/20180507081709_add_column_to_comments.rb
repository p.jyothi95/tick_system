class AddColumnToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :creator_type, :string
    rename_column :comments, :user_id, :creator_id
  end
end
