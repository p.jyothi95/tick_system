class ModifyTicketLogs < ActiveRecord::Migration[5.1]
  def change
    rename_column :ticket_logs, :user_id, :performer_id
    add_column :ticket_logs, :performer_type, :string
  end
end
