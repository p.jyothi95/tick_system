class AddColumnToTodos < ActiveRecord::Migration[5.1]
  def change
    add_column :todos, :creator_type, :string
    rename_column :todos, :user_id, :creator_id
  end
end
