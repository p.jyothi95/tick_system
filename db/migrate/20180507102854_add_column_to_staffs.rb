class AddColumnToStaffs < ActiveRecord::Migration[5.1]
  def change
    add_attachment :staffs, :photo
    add_column :staffs, :is_deleted, :boolean, default:false
  end
end
