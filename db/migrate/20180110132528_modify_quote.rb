class ModifyQuote < ActiveRecord::Migration[5.1]
  def change
    add_column :quotes, :product, :text 
    add_column :quotes, :address, :string
    add_column :quotes, :city, :string
    add_column :quotes, :state, :string
    rename_column :quotes, :category, :string
    remove_column :quotes, :requirement, :text
    add_column :quotes, :preferred_method, :string

  end
end
