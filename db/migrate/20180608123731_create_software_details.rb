class CreateSoftwareDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :software_details do |t|
      t.integer :product_id
      t.string :software_os_name
      t.string :product_key
      t.date :purchase_date
      t.date :expiry_date
      t.timestamps
    end
  end
end
