class CreateHardwareDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :hardware_details do |t|
      t.integer :product_id
      t.string :processor
      t.string :hdd
      t.string :ram
      t.string :serial_no
      t.date :purchase_date
      t.integer :warranty
      t.text :add_on
      t.timestamps
    end
  end
end
