class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :asset_type
      t.string :brand
      t.integer :asset_tag
      t.string :department
      t.integer :quantity
      t.integer :model_no
      t.text :description
      t.timestamps
    end
  end
end
