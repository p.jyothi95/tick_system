class AddFeedbackScoreAndFeedbackCommentToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :feedback_score, :integer
    add_column :tickets, :feedback_comment, :text
  end
end
