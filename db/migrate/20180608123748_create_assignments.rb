class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.integer :product_id
      t.string :location
      t.string :managed_by
      t.string :department
      t.string :used_by
      t.date :assigned_on
      t.attachment :attachment
      t.timestamps
    end
  end
end
