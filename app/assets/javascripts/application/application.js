// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require ./jquery-3.1.1.min
//= require ./jquery-ui.min
//= require rails-ujs
//= require ./tether.min
//= require ./bootstrap.min
//= require plugins/charts/echarts/js/echarts-all.js
//= require plugins/waves/js/waves.min
//= require plugins/slimscroll/js/jquery.slimscroll
//= require plugins/slimscroll/js/jquery.nicescroll.min
//= require plugins/search/js/classie
//= require plugins/notification/js/bootstrap-growl.min
//= require plugins/charts/sparkline/js/jquery.sparkline
//= require plugins/countdown/js/waypoints.min
//= require plugins/countdown/js/jquery.counterup
//= require ./main
//= require plugins/select2/js/select2.full.min.js
//= require plugins/modal/js/classie.js
//= require plugins/modal/js/modalEffects.js
//= require plugins/notification/js/bootstrap-growl.min.js
//= require plugins/charts/chart/js/Chart.js
//= require ../pages/chart-chartjs
//= require ../pages/elements-materialize.js
//= require ../pages/dashboard
//= require ../pages/elements
//= require ../pages/notification
//= require ./menu-sidebar-sticky
//= require ./menu
//= require plugins/datepicker/js/moment-with-locales.min.js
//= require plugins/datepicker/js/bootstrap-material-datetimepicker.js
//= require plugins/todo/js/todo.js
//= require trix

function toggleFullScreen() {
    if (!document.fullscreenElement && // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement) { // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}

$(document).ready(function () {
  $("#view-notifications").click(function(){
      $.ajax("/notifications/mark_as_read", {
        type: 'PUT', 
        success: function(){
          $(".notification-menu .badge").remove();
        }
      });
  });
  $(".loader-bg").fadeOut("slow");

  $('#date, .date').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });

  $(document).on('change', '.todo-checkbox', function(){
    $.ajax('/todos/' + $(this).data('id'), {
      data: {todo: {is_completed: $(this).is(':checked') ? 1 : 0}},
      type: 'PUT'
    })
  });

  $('input:-webkit-autofill').each(function(index, ele){
    $(ele).addClass('md-valid');
  });

  $('.md-input-wrapper input').each(function(index, ele){
    if($(ele).val() != '')
      $(ele).addClass('md-valid');
  });

  $('.add-more-products').click(function(e){
    e.preventDefault();

    var t = $('.products .card-block:last-child').clone();
    t.find('select, input').val('');
    $('.products').append(t);
  });

  $('input.feedback-score').change(function() {
    $('.scores-table td').removeClass('active');
    $('.scores-table td.score-' + $(this).val()).addClass('active');
  }).trigger('change');

  $('.scores-table td').click(function(){
    $('input.feedback-score').val($(this).data('val')).trigger('change');
  });

});
