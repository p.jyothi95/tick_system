//= require rails-ujs
//= require turbolinks
//= require ../application/jquery-3.1.1.min
//= require ../application/tether.min
//= require plugins/waves/js/waves.min
//= require ../application/bootstrap.min
//= require ../application/jarallax
//= require ../application/slick.min
//= require plugins/isotope/js/jquery.isotope
//= require plugins/isotope/js/isotope.pkgd.min
//= require ../pages/landing-page
//= require ../application/common-pages