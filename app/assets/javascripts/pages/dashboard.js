  'use strict';
    $(document).ready(function() {
 dashboard();

 /*Counter Js Starts*/
$('.counter').counterUp({
    delay: 10,
    time: 400
});
/*Counter Js Ends*/

//  Resource bar
    $(".resource-barchart").sparkline([5, 6, 2, 4, 9, 1, 2, 8, 3, 6, 4,2,1,5], {
              type: 'bar',
              barWidth: '15px',
              height: '80px',
              barColor: '#fff',
            tooltipClassname:'abc'
          });


            //function digclock() {
            //    var d = new Date()
            //    var t = d.toLocaleTimeString()

            //    document.getElementById("system-clock").innerHTML = t
            //}

            //setInterval(function() {
            //    digclock()
            //}, 1000)
            
            function setHeight() {
                var $window = $(window);
                var windowHeight = $(window).height();
                if ($window.width() >= 320) {
                    $('.user-list').parent().parent().css('min-height', windowHeight);
                    $('.chat-window-inner-content').css('max-height', windowHeight);
                    $('.user-list').parent().parent().css('right', -300);
                }
            };
            setHeight();

            $(window).on('load',function() {
                setHeight();
            });
        });

 $(window).resize(function() {
        dashboard();
        //  Resource bar
    $(".resource-barchart").sparkline([5, 6, 2, 4, 9, 1, 2, 8, 3, 6, 4,2,1,5], {
              type: 'bar',
              barWidth: '15px',
              height: '80px',
              barColor: '#fff',
            tooltipClassname:'abc'
          });
    });

function dashboard(){

  /*Pie chart*/
  var pieElem = document.getElementById("pieChart");
  var data = {
      labels: [],
      title: {
                display:true,
                text: 'Chart.js'
              },
      datasets: [{
          data: [],
          backgroundColor: [],
          hoverBackgroundColor: []
      }]
  };

  var myPieChart = new Chart(pieElem,{
    type: 'pie',
    data: data
  });

  $('.pie-chart-data-url').change(function(){
    $.ajax($(this).val(), {
      success: function(data, xhr) {
        myPieChart.data.title = data.title;
        myPieChart.data.labels = data.labels;
        myPieChart.data.datasets = data.datasets;
        myPieChart.update();
      }
    });
  }).trigger('change');

  $('.chart-filter').click(function(){
    $('.pie-chart-data-url').val($(this).data('url')).trigger('change');
    $('.chart-title').html($(this).find('span').text());
  });
  
  pc = myPieChart;
};

var pc;
