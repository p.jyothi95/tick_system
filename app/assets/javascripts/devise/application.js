//= require rails-ujs
//= require ../application/jquery-3.1.1.min
//= require ../application/jquery-ui.min
//= require ../application/tether.min
//= require ../application/bootstrap.min
//= require ../pages/elements
//= require plugins/notification/js/bootstrap-growl.min.js
//= require ../pages/notification

$(document).ready(function () {
  $('.md-input-wrapper input:-webkit-autofill').each(function(index, ele){
    $(ele).addClass('md-valid');
  });

  $('.md-input-wrapper input').each(function(index, ele){
    if($(ele).val() != '')
      $(ele).addClass('md-valid');
  });
});
