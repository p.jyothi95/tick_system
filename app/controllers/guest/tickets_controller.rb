class Guest::TicketsController < ApplicationController
  
  layout 'guest'
  def new
    @ticket = Ticket.new
  end

  def create
    @ticket = Ticket.new(ticket_params)
    Ticket.with_performer nil do
      if verify_recaptcha(model: @ticket) && @ticket.save
        flash[:success]="Thank you"
        redirect_to new_user_session_path
      else
        render action: 'new'
      end
    end
  end

  private
    def ticket_params
      params.require(:ticket).permit(:name,:email, :subject, :description, :attachment).tap do |p|
        p[:is_guest] = true
        p[:priority] = 'low'
      end
    end

end
