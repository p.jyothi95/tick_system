class PagesController < ApplicationController

  def locked
    @resource = session['lock']['scope'].capitalize.constantize.find session['lock']['id']
    render layout: 'devise'
  end
  
end
