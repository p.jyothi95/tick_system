class ProductsController < ApplicationController
  def new
    @product = Product.new
    @hardware_detail = HardwareDetail.new
    @software_detail = SoftwareDetail.new
    @assignment = Assignment.new
  end

  def create
    @product = Product.new(product_params)

    @hardware_detail = HardwareDetail.new(hardware_product_params)

    @software_detail = SoftwareDetail.new(software_product_params)
    

    @assignment = Assignment.new(assignment_params)
    

    if @product.valid? and @hardware_detail.valid? and @software_detail.valid? and @assignment.valid?
      @product.save

      @hardware_detail.product_id = @product.id
      @hardware_detail.save

      @software_detail.product_id = @product.id
      @software_detail.save

      @assignment.product_id = @product.id
      @assignment.save

    redirect_to products_path

    else 
    render action: "new"
    end

  end

  def edit
    @product = Product.find(params[:id])
    @hardware_detail = HardwareDetail.find_by(product_id: params[:id])
    @software_detail = SoftwareDetail.find_by(product_id: params[:id])
    @assignment = Assignment.find_by(product_id: params[:id])
  end


  def index
    @products = Product.all

    respond_to do |format|
      format.html
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"products-list.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
      format.pdf do
        render pdf: "products-list", disposition: 'attachment', layout: 'pdf'
      end
    end

  end

  def search
    @products = Product.all
    if params[:search][:query].present?
      query = '%' + params[:search][:query] + '%'
      @products = @products.where("name LIKE ? OR brand LIKE ?", query, query)
    end
    render action: :index
  end

  def show
    @product = Product.find params[:id]
  end

  def update
    @product = Product.find params[:id]
    @product.update_attributes! product_params

    @hardware_detail = HardwareDetail.find_by(product_id: params[:id])
    @hardware_detail.update_attributes! hardware_product_params

    @software_detail = SoftwareDetail.find_by(product_id: params[:id])
    @software_detail.update_attributes! software_product_params

    @assignment = Assignment.find_by(product_id: params[:id])
    @assignment.update_attributes! assignment_params

    flash[:success]="Successfully updated"

   
    redirect_to product_path(@product)
  end

  def destroy
    product= Product.find(params[:id])
    product.destroy
    redirect_to products_path
  end
  
  private
    def product_params
      params.require(:product).permit(:name, :asset_type, :brand, :asset_tag, :quantity,:model_no, :description)
    end

    def hardware_product_params
      params.require(:hardware_detail).permit(:processor, :hdd, :ram, :serial_no, :purchase_date,:warranty, :add_on)
    end

    def software_product_params
      params.require(:software_detail).permit(:software_os_name, :product_key, :purchase_date, :expiry_date)
    end

    def assignment_params
      params.require(:assignment).permit(:location, :managed_by, :department, :used_by, :assigned_on,:attachment)
    end


end
