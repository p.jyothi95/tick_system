class NotificationsController < ApplicationController

  def mark_as_read
    if current_staff.present?
      @notifications= Notification.where(notifiable_id: current_staff.id, notifiable_type: 'Staff').update_all(is_read: true)
      render json: {success: true}.to_json
    else
      @notifications= Notification.where(notifiable_id: current_user.id, notifiable_type: 'User').update_all(is_read: true)
      render json: {success: true}.to_json
    end
  end

  def show
    @notification = Notification.find(params[:id])
      if @notification.notifiable_type == "Staff" &&  (@notification.notification_type == "ticket_created" or @notification.notification_type ==  "ticket_updated")
        redirect_to staff_ticket_path(@notification.reference_id)
      elsif @notification.notifiable_type == "User" &&  (@notification.notification_type == "ticket_created" or @notification.notification_type ==  "ticket_updated")
        redirect_to ticket_path(@notification.reference_id)
      elsif @notification.notifiable_type == "Staff" &&  @notification.notification_type == "comment_created"
        redirect_to staff_ticket_path(@notification.reference.ticket_id, :anchor => "comment-#{@notification.reference.id}")
      elsif @notification.notifiable_type == "User" &&  @notification.notification_type == "comment_created"
        redirect_to ticket_path(@notification.reference.ticket_id, :anchor => "comment-#{@notification.reference.id}")
      else @notification.notifiable_type == "Staff" &&  @notification.notification_type == "quote_created" 
        redirect_to staff_quote_path(@notification.reference_id)
      end
  end

end
