class TodosController < ApplicationController
  
  def create
    todo =Todo.new(todo_params)
    todo.creator=(current_user || current_staff)
    todo.save
    @todos = todo.creator.todos.where(is_completed: false).order(created_at: :asc)
  end

  def update
    @todo = Todo.find(params[:id])
    @todo.update_attributes(todo_params)
    render body: nil
  end

  private
    def todo_params
      params.require(:todo).permit(:description, :is_completed)
    end
end
