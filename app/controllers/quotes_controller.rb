class QuotesController < ApplicationController

  def new
    @quote = Quote.new
    @quote.name = current_user.name
  end

  def create
    @quote =Quote.new(quote_params)
    @quote.user_id = current_user.id
    if @quote.save
      redirect_to thankyou_screen_path
    else
      render action: 'new'
    end
  end

  private
    def quote_params
      params.require(:quote).permit(:name, :address, :city, :state, :phone, :email, :preferred_method, :description, product: [:type, :quantity] )
    end
    
end
