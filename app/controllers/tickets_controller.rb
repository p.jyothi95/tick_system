class TicketsController < ApplicationController

  before_action :authenticate_user!

  def new
    @ticket = current_user.tickets.new(priority: 'low', name: current_user.name)
  end

  def create
    Ticket.with_performer current_user do
      @ticket = current_user.tickets.new(ticket_params)
      if @ticket.save
        redirect_to tickets_path
      else
        render action: 'new'
      end
    end
  end

  def index
    if current_user.is_manager?
      @tickets = Ticket.joins(:user).where("users.client_id": current_user.client_id)
    else
      @tickets = Ticket.where(user_id: current_user.id)
    end
    
    if params[:status].present?
      if params[:status] == "unassigned"
        @tickets = Ticket.where(staff: nil)
      elsif params[:status] != "all"
        @tickets = @tickets.where(status: params[:status])
      end
    else
      @tickets = @tickets.where(status: nil).order(id: :desc)
    end
    respond_to do |format|
      format.html
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"tickets-list.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
      format.pdf do
        render pdf: "tickets-list", disposition: 'attachment', layout: 'pdf'
      end
    end
  end

  def show
    @ticket = Ticket.find params[:id]
  end

  def search
    @tickets = Ticket.where(user_id: current_user.id)
    @tickets = @tickets.where(status: params[:search][:status]) if params[:search][:status].present?
    @tickets = @tickets.where(staff_id: params[:search][:staff_id]) if params[:search][:staff_id].present?
    @tickets = @tickets.where(priority: params[:search][:priority]) if params[:search][:priority].present?

    if params[:search][:query].present?
      query = '%' + params[:search][:query] + '%'
      @tickets = @tickets.where("name LIKE ? OR subject LIKE ?", query, query)
    end
    render action: :index
  end

  def feedback_submit
    @ticket = Ticket.find params[:id]
    Ticket.with_performer current_user do
      @ticket.update_attributes(feedback_params)
      params[:success] = 'Feedback received'
      redirect_to ticket_path(@ticket)
    end
  end

  private
    def ticket_params
      params.require(:ticket).permit(:name, :subject, :description, :priority, :attachment,:category, :source, :service_type, :expected_closing_date)
    end

    def feedback_params
      params.require(:ticket).permit(:feedback_score, :feedback_comment)
    end

end
