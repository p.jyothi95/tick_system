class CommentsController < ApplicationController
  before_action :find_ticket

  def create
    @comment = @ticket.comments.new comment_params
    @comment.creator=(current_user || current_staff)
    

    if @comment.save
      flash[:success] = "Comment added successfuly"
      @ticket.log!('commented', current_user|| current_staff, @comment)
    else
      flash[:error] = "Unable to add comment"
    end

    if current_staff.present?
    redirect_to staff_ticket_path(@ticket)
  else
    redirect_to ticket_path(@ticket)
  end
  end

  private
    def find_ticket
      @ticket = Ticket.find params[:ticket_id]
    end

    def comment_params
      params.require(:comment).permit(:description)
    end
end
