class Staff::DashboardController < Staff::BaseController

  def show
    @tickets =Ticket
    @todos = current_staff.todos.where(is_completed: false).order(created_at: :asc)
  end
  
  def chart_data
    data = prepare_data(tickets_scope)
    colors = data_colors[0..data.count-1]

    op = {
      labels: data.keys,
      datasets: [{
        label: "Ticket Stats",
        data: data.values,
        backgroundColor: colors,
        hoverBackgroundColor: colors
      }]
    }

    render json: JSON.dump(op)
  end

  private
  def tickets_scope
    tickets = Ticket
    tickets = case params[:filter]
    when 'open'
      tickets.open
    when 'overdue'
      tickets.where("status=? and expected_closing_date < ?", "in_progress", Date.today)
    when 'in_progress'
      tickets.where(status: "in_progress")
    when 'due_today'
      tickets.where("status != ? and expected_closing_date = ?", "closed", Date.today)
    when 'unassigned'
      tickets.where(staff: nil)
    when 'closed'
      tickets.where(status: "closed")
    else
      tickets
    end

    tickets
  end

  def prepare_data(scope)
    op = {}
    Ticket::CATEGORIES.each do |c|
      count = scope.where(category: c).count
      op[c] = count if count > 0
    end
    if (count = scope.where('category IS NULL OR category NOT IN (?)', Ticket::CATEGORIES).count) > 0
      op['Others'] = count
    end

    op
  end

  def data_colors
    [
      "#222222",
      "#b8b8b8",
      "#00dac7",
      "#5C5C5C",
      "#ff5252",
      "#2196F3",
      "#4CAF50",
      "#FF0084",
      "#f57c00"
    ].shuffle
  end
end
