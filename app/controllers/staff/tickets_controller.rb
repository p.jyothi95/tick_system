class Staff::TicketsController < Staff::BaseController

  def new
    @ticket = current_staff.tickets.new(priority: 'low', name: current_staff.name)
  end

  def create
    Ticket.with_performer current_staff do
      @ticket = current_staff.tickets.new(t_params)
      if @ticket.save
        redirect_to staff_tickets_path
      else
        render action: 'new'
      end
    end
  end


  def index
    @tickets =Ticket.all.order(id: :desc)
    if params[:status].present?
      if params[:status] == "unassigned"
        @tickets = Ticket.where(staff_id: nil)
      elsif params[:status] == "overdue"
        @tickets = @tickets.where("status != ? and expected_closing_date < ?", "closed", Date.today)
      elsif params[:status] == "due_today"
        @tickets = @tickets.where("status != ? and expected_closing_date = ?", "closed", Date.today)
      elsif
        @tickets = @tickets.where(status: params[:status])
      end
    else
      @tickets = @tickets.where(status: nil)
    end
    respond_to do |format|
      format.html
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"tickets-list.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
      format.pdf do
        render pdf: "tickets-list", disposition: 'attachment', layout: 'pdf'
      end
    end
  end

  def show
    @ticket = Ticket.find params[:id]
    @quote = Quote.find_by_ticket_id(@ticket.id) 
    @staffs = Staff.all
  end

  def update
    @ticket = Ticket.find params[:id]
    Ticket.with_performer current_staff do
      @ticket.update_attributes! ticket_params
    end
    
    redirect_to staff_ticket_path(@ticket)
  end

  def feedback_submit
    @ticket = Ticket.find params[:id]
    Ticket.with_performer current_user do
      @ticket.update_attributes(feedback_params)
      params[:success] = 'Feedback received'
      redirect_to ticket_path(@ticket)
    end
  end

  def search
    @tickets = Ticket.where("1=1")
    @tickets = @tickets.where(status: params[:search][:status]) if params[:search][:status].present?
    @tickets = @tickets.where(staff_id: params[:search][:staff_id]) if params[:search][:staff_id].present?
    @tickets = @tickets.where(priority: params[:search][:priority]) if params[:search][:priority].present?
    @tickets = @tickets.where(category: params[:search][:category]) if params[:search][:category].present?
    @tickets = @tickets.joins(:user).where("users.client_id": params[:search][:client_id]) if params[:search][:client_id].present?

    if params[:search][:query].present?
      query = '%' + params[:search][:query] + '%'
      @tickets = @tickets.where("name LIKE ? OR subject LIKE ?", query, query)
    end

    render action: :index
  end

  private
    def ticket_params
      op = params.require(:ticket).permit(:status, :priority, :staff_id)
      op[:status] = nil if op[:status] == ""
      op
    end

    def t_params
      params.require(:ticket).permit(:name, :subject, :description, :priority, :attachment,:category, :source, :service_type, :user_id, :expected_closing_date)
    end

    def feedback_params
      params.require(:ticket).permit(:feedback_score, :feedback_comment)
    end
end
