class Staff::QuotesController < ApplicationController

  def create_ticket
    quote = Quote.find params[:id]
    if !quote.ticket_id.present?
      Ticket.with_performer current_staff do
        ticket = Ticket.new
        ticket.name = quote.name
        ticket.subject = "Quote from #{quote.name}"
        ticket.user_id = quote.user_id
        ticket.priority = 'medium'
        ticket.description = quote.description
        ticket.source = quote.preferred_method
        ticket.save!
        quote.ticket_id = ticket.id
        quote.save
      end
      redirect_to staff_tickets_path
    end
  end

  def index
    @quotes = Quote.all.order(id: :desc)
  end

  def show
    @quote = Quote.find params[:id]
  end
  
end
