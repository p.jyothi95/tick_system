class Staff::UsersController < ApplicationController

  def new
    @user = User.new 
    @client= Client.find(params[:client_id])
  end

  def show
    @client= Client.find(params[:client_id])
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
    @client= Client.find(params[:client_id])
  end

  def update
    @client= Client.find(params[:client_id])
    @user= User.find(params[:id])
      if @user.update_attributes(user_params)
        flash[:success]="Successfully updated"
        redirect_to staff_client_user_path()
      else
        render 'edit'
    end
  end

  def create
    @client= Client.find(params[:client_id])
    @user = User.new(user_params)
    @user.client_id = @client.id
    if @user.save
      redirect_to staff_client_path(@client)
    else
      render action: 'new'
    end
  end

  def search
  @client= Client.find(params[:client_id])
  @users = User.where(client_id: @client.id)
  if params[:search][:query].present?
    query = '%' + params[:search][:query] + '%'
    @users = @users.where("name LIKE ? OR email LIKE ?", query, query)
  end
  render template: 'staff/clients/show'
  end

  def destroy
    @client= Client.find(params[:client_id])
    @user = User.find(params[:id])
    @user.is_deleted = true
    @user.save
    redirect_to staff_client_path(@client)
  end

  private
    def user_params
      op = params.require(:user).permit(:photo, :name, :email, :password, :password_confirmation, :role, :is_deleted)
      if op[:password].blank? and op[:password_confirmation].blank?
        op.delete :password
        op.delete :password_confirmation
      end
      return op
    end
    
end
