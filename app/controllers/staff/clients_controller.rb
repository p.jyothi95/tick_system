class Staff::ClientsController < ApplicationController

  def new
    @client = Client.new 
  end

  def index
    @clients = Client.where(is_deleted: false).order(name: :asc)
  end

  def edit
    @client= Client.find(params[:id])
  end
  
  def create
    @client = Client.new(client_params)
    if @client.save
      redirect_to staff_clients_path
    else
      render action: 'new'
    end
  end

  def update
    @client= Client.find(params[:id])
    if @client.update_attributes(client_params)
      flash[:success]="Successfully updated"
      redirect_to staff_client_path(@client)
    else
      render 'edit'
    end
  end

  def show
    @client= Client.find(params[:id])
     @users = @client.users.where(is_deleted: false).order(name: :asc)
    @user = User.new
  end

  def destroy
    @client= Client.find(params[:id])
    @client.is_deleted = true
    @client.save
    redirect_to staff_clients_path
  end

  private
    def client_params
      params.require(:client).permit(:name, :address, :logo, :phone, :email, :website, :is_deleted)
    end
    
end
