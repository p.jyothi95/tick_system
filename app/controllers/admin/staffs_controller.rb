class Admin::StaffsController < Admin::BaseController

  def new
    @staff = Staff.new 
  end

  def edit
    @staff= Staff.find(params[:id])
  end

  def update
    @staff= Staff.find(params[:id])
    if @staff.update_attributes(staff_params)
      flash[:success]="Successfully updated"
      redirect_to admin_staffs_path(@staff)
    else
      render 'edit'
    end
  end

  def index
     @staffs = Staff.where(is_deleted: false).order(name: :asc)
  end

  def destroy
    @staff= Staff.find(params[:id])
    @staff.update_attributes is_deleted: true
    redirect_to admin_staffs_path
  end

  def create
    @staff = Staff.new(staff_params)
    if @staff.save
      redirect_to admin_staffs_path
    else
      render action: 'new'
    end
  end

  private
    def staff_params
      params.require(:staff).permit(:name, :email, :password, :password_confirmation, :role, :is_deleted).tap do |p|
        if p[:password].empty? and p[:password_confirmation].empty?
          p.delete :password
          p.delete :password_confirmation
        end
      end
    end
end
