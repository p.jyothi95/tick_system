class Admin::BaseController < ApplicationController

  before_action :authenticate_staff!
  before_action :require_admin!

  private
    def require_admin!
      return true if current_staff.is_admin?
      deny_access!
    end
    
end
