class AccountController < ApplicationController

  def edit
    @user = current_user
  end
  
  def update
    @user = current_user
    if @user.update_attributes(user_params)
      sign_in(@user, :bypass => true)
      flash[:success]="Successfully updated"
      redirect_to account_path
    else
      render 'edit'
    end
  end

  def show
    @user = current_user
  end

  private
    def user_params
      params.require(:user).permit(:name, :password, :password_confirmation, :photo).tap do |p|
        if p[:password].empty? and p[:password_confirmation].empty?
          p.delete :password
          p.delete :password_confirmation
        end
      end
    end
    
end
