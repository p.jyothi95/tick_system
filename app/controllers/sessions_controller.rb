class SessionsController < Devise::SessionsController
  def new
    super
  end

  def create
    ao = auth_options
    if User.exists?(email: (params[:user] || params[:staff])[:email])
      ao[:scope] = :user
      params[:user] ||= params.delete :staff
      @devise_mapping = request.env["devise.mapping"] = Devise.mappings[:user]
    else
      ao[:scope] = :staff
      params[:staff] ||= params.delete :user
      @devise_mapping = request.env["devise.mapping"] = Devise.mappings[:staff]
    end

    self.resource = warden.authenticate!(ao)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    if !session[:return_to].blank?
      redirect_to session[:return_to]
      session[:return_to] = nil
    else
      respond_with resource, :location => after_sign_in_path_for(resource)
    end
  end
end
