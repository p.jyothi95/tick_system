class Manager::UsersController < ApplicationController

  def new
    @user = User.new
  end

  def index
    @user = User.new
    @users = User.where(client_id: current_user.client_id, is_deleted: false).order(name: :asc)
  end

  def create
    @user = User.new(user_params)
    @user.client_id = current_user.client_id
    if @user.save
      redirect_to manager_users_path
    else
      render action: 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user= User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success]="Successfully updated"
      redirect_to manager_users_path()
    else
      render 'edit'
    end
  end

  def destroy
    @user= User.find(params[:id])
    @user.update_attributes is_deleted: true
    redirect_to manager_users_path
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :role)
    end
    
end
