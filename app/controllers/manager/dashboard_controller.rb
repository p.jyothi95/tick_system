class Manager::DashboardController < ApplicationController

  def show
    @todos = Todo.where(creator:  current_user, is_completed: false)
    @tickets =Ticket.joins(:user).where("users.client_id": current_user.client_id)
    @users = User.where(client_id: current_user.client_id)
  end

  def chart_data
    data = prepare_data(tickets_scope)
    colors = data_colors[0..data.count-1]

    op = {
      labels: data.keys,
      datasets: [{
        label: "Ticket Stats",
        data: data.values,
        backgroundColor: colors,
        hoverBackgroundColor: colors
      }]
    }

    render json: JSON.dump(op)
  end

  private
  def tickets_scope
    tickets = Ticket.joins(:user).where("users.client_id": current_user.client_id)
    tickets = case params[:filter]
    when 'open'
      tickets.open
    when 'in_progress'
      tickets.where(status: "in_progress")
    when 'all'
      Ticket.joins(:user).where("users.client_id": current_user.client_id)
    when 'unassigned'
      tickets.where(staff: nil)
    when 'closed'
      tickets.where(status: "closed")
    else
      tickets
    end

    tickets
  end

  def prepare_data(scope)
    op = {}
    Ticket::CATEGORIES.each do |c|
      count = scope.where(category: c).count
      op[c] = count if count > 0
    end
    if (count = scope.where('category IS NULL OR category NOT IN (?)', Ticket::CATEGORIES).count) > 0
      op['Others'] = count
    end

    op
  end

  def data_colors
    [
      "#222222",
      "#b8b8b8",
      "#00dac7",
      "#5C5C5C",
      "#ff5252",
      "#2196F3",
      "#4CAF50",
      "#FF0084",
      "#f57c00"
    ].shuffle
  end
  
end
