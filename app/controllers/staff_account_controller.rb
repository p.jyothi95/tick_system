class StaffAccountController < ApplicationController

  def edit
    @staff = current_staff
  end
  
  def update
   @staff = current_staff
    if @staff.update_attributes(staff_params)
      sign_in(@staff, :bypass => true)
      flash[:success]="Successfully updated"
      redirect_to staff_account_path
    else
      render 'edit'
    end
  end

  def show
    @staff = current_staff
  end

  private
    def staff_params
      params.require(:staff).permit(:name, :password, :password_confirmation, :photo).tap do |p|
        if p[:password].empty? and p[:password_confirmation].empty?
          p.delete :password
          p.delete :password_confirmation
        end
      end
    end
end
