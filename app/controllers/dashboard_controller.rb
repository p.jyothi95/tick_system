class DashboardController < ApplicationController

  before_action :authenticate_user!

   def show
    @todos = Todo.where(creator:  current_user, is_completed: false)
  end

end
