class ApplicationController < ActionController::Base

  require 'csv'
  
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_timezone

  before_action :set_notifications

  layout :set_layout

  private
    def after_sign_out_path_for(resource_or_scope)
      if params[:lock]
        session[:lock] = {
          scope: resource_or_scope,
          id: params[:id]
        }
        flash[:notice] = 'Screen locked'
        locked_screen_path
      else
        super
      end
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password, :password_confirmation])
    end

    def set_layout
      devise_controller? ? (resource_name == :user ? 'devise' : 'devise_staff') : 'application'
    end

    def set_timezone
      Time.zone = "Mumbai"
    end

    def after_sign_in_path_for(resource)
      staff_signed_in? ? staff_dashboard_path : (resource.is_manager? ? manager_dashboard_path : root_path)
    end


    def deny_access!
      flash[:error] = "You do not have permission to access the page"
      redirect_to root_path
      false
    end 

    def set_notifications
      if current_staff.present?
        @notifications = Notification.where(notifiable_id: current_staff.id, notifiable_type: 'Staff').order(created_at: :desc)
      end
      if current_user.present?
        @notifications = Notification.where(notifiable_id: current_user.id, notifiable_type: 'User').order(created_at: :desc)
      end
    end

end
