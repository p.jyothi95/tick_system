class SoftwareDetail < ApplicationRecord
  belongs_to :product, optional: true
   validates :software_os_name, presence: true

end
