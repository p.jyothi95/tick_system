class Ticket < ApplicationRecord
  serialize :extra, JSON

  PRIORITIES = ['low', 'medium', 'high']
  STATUSES = {nil => 'Open', 'in_progress' => 'In progress', 'closed' => 'Closed'}
  CATEGORIES = ['Laptop', 'Desktop', 'Printer', 'Scanner', 'Xerox', 'CCTV', 'UPS', 'Softwares', 'Network', 'Server Monitoring']
  SOURCES = ['Mail', 'Phone', 'Walk-in', 'Tool']
  SERVICE_TYPES = ['Service', 'Maintanance', 'New Product']

  validates :subject, :description, presence: true
  validates :status, inclusion: STATUSES.keys
  validates :user, presence: true, unless: :is_guest?
  validates :priority, inclusion: PRIORITIES, unless: :is_guest?
  #validates :category, inclusion: CATEGORIES, unless: :is_guest?
  #validates :source, inclusion: SOURCES, unless: :is_guest?
  #validates :service_type, inclusion: SERVICE_TYPES, unless: :is_guest?
  do_not_validate_attachment_file_type :attachment

  has_attached_file :attachment

  belongs_to :user, optional: :is_guest?
  belongs_to :staff, optional: true
  has_many :comments
  has_many :ticket_logs
  has_many :notifications, as: :reference

  scope :open, -> {where(status: nil)}

  after_create :log_created, :send_email, :notify_created
  after_update :log_updated, :notify_updated

  def self.with_performer(performer)
    @@performer = performer
    yield
  end

  def log!(action, performer, reference = nil, details = {})
    ticket_logs.create! action: action, performer: performer, reference: reference, details: details
  end

  def number
    "IHT#{id}"
  end

  def email
    extra && extra['email']
  end

  def email=(value)
    self.extra ||= {}
    self.extra['email'] = value
  end

  private
    def log_created
      log!('created', @@performer, nil, attributes)
    end

    def log_updated
      log!('updated', @@performer, nil, changes)
    end

    def send_email
      TicketMailer.notify_user(id).deliver
    end

    def notify_created
      Staff.all.each do |staff|
        notification = Notification.new
        notification.notification_type = "ticket_created"
        notification.reference_type = "Ticket"
        notification.reference_id = self.id
        notification.notifiable_type = "Staff"
        notification.notifiable_id = staff.id
        notification.save!
      end

      User.where(role: 'manager', client_id: self.user.client_id).each do |user|
        notification = Notification.new
        notification.notification_type = "ticket_created"
        notification.reference_type = "Ticket"
        notification.reference_id = self.id
        notification.notifiable_type = "User"
        notification.notifiable_id = user.id
        notification.save!
      end
    end

    def notify_updated
      User.where(role: 'manager', client_id: self.user.client_id).each do |user|
        notification = Notification.new
        notification.notification_type = "ticket_updated"
        notification.reference_type = "Ticket"
        notification.reference_id = self.id
        notification.notifiable_type = "User"
        notification.notifiable_id = user.id
        notification.detail = self.changes
        notification.save!
      end   

      related_people = []
      related_people << self.user
      related_people << self.staff
      related_people.each do |rp|
        if (rp != @@performer)
          notification = Notification.new
          notification.notification_type = "ticket_updated"
          notification.reference_type = "Ticket"
          notification.reference_id = self.id
          notification.notifiable_type = rp.class.to_s
          notification.notifiable_id = rp.id
          notification.detail = self.changes
          notification.save!
        end
      end
    end

end
