class Quote < ApplicationRecord
  validates :name, :email, presence: true
  has_many :notifications, as: :reference
  belongs_to :user

  after_create :notify_created

  serialize :product, JSON

  def notify_created
    Staff.all.each do |s|
      notification = Notification.new
      notification.notification_type = "quote_created"
      notification.reference_type = "Quote"
      notification.reference_id = self.id
      notification.notifiable_type = "Staff"
      notification.notifiable_id = s.id
      notification.save!
    end
  end

end
