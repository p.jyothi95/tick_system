class Comment < ApplicationRecord
  belongs_to :ticket
  has_many :ticket_logs, as: :reference
  has_many :notifications, as: :reference
  belongs_to :creator, polymorphic: true, optional: true


  after_create :notify_created

  def notify_created
    User.where(role: 'manager', client_id: self.ticket.user.client_id).each do |user|
      notification = Notification.new
      notification.notification_type = "comment_created"
      notification.reference_type = "Comment"
      notification.reference_id = self.id
      notification.notifiable_type = "User"
      notification.notifiable_id = user.id
      notification.save!
    end 

    related_people = [ ]
    related_people << self.ticket.user
    related_people << self.ticket.staff
    related_people.each do |rp|
      if (rp != self.creator)
        notification = Notification.new
        notification.notification_type = "comment_created"
        notification.reference_type = "Comment"
        notification.reference_id = self.id
        notification.notifiable_type = rp.class.to_s
        notification.notifiable_id = rp.try(:id)
        notification.save!
      end
    end
  end

end
