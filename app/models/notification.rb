class Notification < ApplicationRecord
  belongs_to :reference, polymorphic: true, optional: true
  belongs_to :notifiable, polymorphic: true, optional: true
  TYPES = ['comment_created', 'ticket_created', 'ticket_updated', 'ticket_assigned', 'quote_created']

  serialize :details, JSON

  validates :notification_type, inclusion: TYPES
end
