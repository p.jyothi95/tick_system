class Todo < ApplicationRecord
  validates :description, :description, presence: true
  belongs_to :creator, polymorphic: true, optional: true
end
