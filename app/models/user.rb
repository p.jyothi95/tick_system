class User < ApplicationRecord
  ROLES = { 'manager' => 'Manager', 'end_user' => 'User'}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tickets
  has_many :quotes
  belongs_to :client
  has_many :todos, as: :creator
  has_many :comments, as: :creator
  has_many :notifications, as: :notifiable

  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/noimg.png"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  
  validates :role, inclusion: ROLES.keys


  ROLES.each do |k, v|
    define_method "is_#{k}?" do
      role == k
    end
  end
end
