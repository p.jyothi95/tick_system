class TicketLog < ApplicationRecord
  ACTIONS = {'commented' => 'Commented', 'created' => 'Created', 'updated' => 'Updated', 'closed' => 'Closed', 'assigned' => 'Assigned', 'reassigned' => 'Re-assigned'}

  serialize :details, JSON

  validates :action, presence: true, inclusion: ACTIONS.keys

  belongs_to :ticket
  belongs_to :performer, polymorphic: true, optional: true
  belongs_to :reference, polymorphic: true, optional: true
end
