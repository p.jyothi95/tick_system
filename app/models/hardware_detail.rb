class HardwareDetail < ApplicationRecord
  belongs_to :product, optional: true
  validates :serial_no, presence: true
end
