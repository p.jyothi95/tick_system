class Product < ApplicationRecord
   validates :name, :asset_type, :brand, :asset_tag, presence: true

   has_one :assignment
   has_one :hardware_detail
   has_many :software_details
end
