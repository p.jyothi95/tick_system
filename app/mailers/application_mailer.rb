class ApplicationMailer < ActionMailer::Base
  default from: 'info@infoheal.com'
  layout 'mailer'
end
