class TicketMailer < ApplicationMailer
  def notify_user(ticket_id)
    @ticket = Ticket.find ticket_id
    to = @ticket.is_guest? ? @ticket.email : @ticket.user.email

    mail to: to,
         subject: "Ticket created - #{@ticket.number}"
  end
end