module ApplicationHelper
  def landing_path
    current_user.present? ? root_path : staff_dashboard_path
  end
end
